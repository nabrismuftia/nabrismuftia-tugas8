import React from 'react';
import {View, Text} from 'react-native';
import {TextRegular, TextBold, TextMedium} from '../../component/global/Text';
import {Colors} from '../../styles/Index';
import {Header} from '../../component/global/Headers';
import {InputText} from '../../component/global/InputText';
// import {NumberFormatter} from '../../utils/Helpher';
// import {Icon} from 'react-native-vector-icons/AntDesign';

const Login = ({navigation, route}) => {
  return (
    <View style={{flex: 1, backgroundColor: '#fff'}}>
      <Header
        title="Login"
        onPress={() => navigation.goBack()}
        isBoldTitle={true}
        titleSize={18}
        backgroundHeader={Colors.PRIMARY}
        iconBackColor={Colors.WHITE}
        titleColor={Colors.WHITE}
      />

      {/* <Icon name="close" size={18} /> */}
      <Text>Login Screen</Text>
      <TextRegular
        // text={'${NumberFormatter(100000000, 'Rp. ' )}'}
        color={Colors.PRIMARY}
      />
      <TextRegular
        text="Halo, kambing apa yang enak? ini text component global"
        color={Colors.PRIMARY}
      />
      <TextMedium
        text="text dari component global yang medium kambing, kambing apa yang enak?"
        color={Colors.DEEPORANGE}
      />
      <TextBold
        text="kambing, kambing apa yang enak, ini adalah text global tapi bold"
        color={Colors.BLACK}
      />

      <InputText
        style={{
          width: '90%',
          alignSelf: 'center',
          marginTop: 20,
        }}
        placeholderText="Masukkan Email"
        keyboardType="email-address"
      />
      <InputText
        style={{
          width: '90%',
          alignSelf: 'center',
          marginTop: 20,
        }}
        placeholderText="Masukkan Nomor Telephone"
        keyboardType="number-pad"
      />
      <InputText
        style={{
          width: '90%',
          alignSelf: 'center',
          marginTop: 20,
        }}
        placeholderText="Masukkan Password Telephone"
        isPassword={true}
      />
    </View>
  );
};

export default Login;
