import React, {useState} from 'react';
import {View, TouchableOpacity, StyleSheet} from 'react-native';
import {
  TextRegular,
  TextBold,
  TextMedium,
  Header,
  InputText,
} from '../../component/global/Text';
import {Colors} from '../../styles/Index';
import ModalBottom from '../../component/modal/ModalBottom';

const LogindenganModal = ({navigation, route}) => {
  const [modalBottom, setModalBottom] = useState(false);
  const [modalCenter, setModalCenter] = useState(false);
  return (
    <View style={{flex: 1, backgroundColor: '#fff'}}>
      <TouchableOpacity
        style={styles.btnShowModalBottom}
        onPress={() => setModalBottom(true)}>
        <TextBold text="Modal Bottom" size={16} color="#fff" />
      </TouchableOpacity>
      <TouchableOpacity
        style={[
          styles.btnShowModalBottom,
          {
            backgroundColor:
              // Colors.DEEPORANGE
              '#ff5722',
          },
        ]}>
        <TextBold text="Modal Center" size={16} color="#fff" />
      </TouchableOpacity>

      <ModalBottom
        show={modalBottom}
        onClose={() => setModalBottom(false)}
        title="Modal Bottom">
        <View>
          <TextBold text="Ini Text Bold" />
          <TextRegular text="Ini Text Reguler" style={{marginVertical: 20}} />
          <TextMedium text="Ini Text Medium" />
        </View>
      </ModalBottom>
    </View>
  );
};
const styles = StyleSheet.create({
  btnShowModalBottom: {
    width: '35%',
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 10,
    borderRadius: 6,
    backgroundColor: Colors.PRIMARY,
    alignSelf: 'center',
    marginTop: 20,
  },
});
export default LogindenganModal;
