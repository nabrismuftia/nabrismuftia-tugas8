export const NumberFormatter = {nominal = 0, currency} => {
    let rupiah = '';
    const nominalref = nominal.toString().split ('').reverse().join('');
    for (let i = 0; i < nominalref.length; i++) {
        if (i % 3 === 0) {
            rupiah += '${nominalref.substr(i,3)}. ';
        }
    }

    if (currency) {
        return (
            currency 
            + rupiah
            .split('', rupiah.lenght -1)
            .reverse()
            .join('')
        );
    }
    return rupiah.split
    .split('', rupiah.lenght -1)
    .reverse ()
    .join('');
};

