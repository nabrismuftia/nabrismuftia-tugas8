import React from 'react';
import {
  View,
  Text,
  ImageBackground,
  Image,
  TextInput,
  TouchableOpacity,
} from 'react-native';

const Home = ({}) => {
  const Separator = () => (
    <View
      style={{
        width: '100%',
        height: 1,
        backgroundColor: '#d3e1ce',
        marginVertical: 5,
      }}
    />
  );

  return (
    // All
    <View style={{flex: 1, backgroundColor: '#dadada'}}>
      {/* Sisi Biru Atas */}
      <Image
        style={{height: 380, width: '100%'}}
        source={require('../asset/img-header.jpg')}
      />
      <View style={{flexDirection: 'row', marginTop: -340}}>
        <View style={{flexDirection: 'column', marginHorizontal: 15}}>
          <Text style={{fontSize: 12, color: 'white'}}>Kepala Cabang</Text>
          <Text style={{fontSize: 14, color: 'white'}}>Cabang Bintaro</Text>
        </View>
        <Image
          style={{
            height: 30,
            width: 30,
            alignSelf: 'center',
            marginLeft: 190,
            tintColor: 'white',
          }}
          source={require('../asset/icon-lonceng.png')}
        />
      </View>

      {/* Kotak Putih Atas  */}
      <View
        style={{
          backgroundColor: 'white',
          width: '90%',
          height: 250,
          alignSelf: 'center',
          marginTop: 15,
          borderRadius: 10,
        }}>
        <Text style={{marginVertical: 10, marginHorizontal: 10}}>
          1 Sep - 12 September 2020
        </Text>
        <Separator />

        <View style={{flexDirection: 'row'}}>
          {/* Kotak Abu 1 dari 4 */}
          <View
            style={{
              margin: 10,
              width: 140,
              height: 70,
              borderRadius: 5,
              backgroundColor: '#eee',
            }}>
            <View style={{flexDirection: 'row', marginVertical: 5}}>
              <View style={{backgroundColor: 'orange', width: 3, height: 20}} />
              <Text style={{marginHorizontal: 5}}>Cek Harga</Text>
            </View>
            <Text
              style={{marginHorizontal: 10, fontSize: 20, fontWeight: 'bold'}}>
              32
            </Text>
          </View>

          {/* Kotak Abu 2 dari 4 */}
          <View
            style={{
              margin: 10,
              width: 140,
              height: 70,
              borderRadius: 5,
              backgroundColor: '#eee',
            }}>
            <View style={{flexDirection: 'row', marginVertical: 5}}>
              <View style={{backgroundColor: 'orange', width: 3, height: 20}} />
              <Text style={{marginHorizontal: 5}}>Kotak Appraisal</Text>
            </View>
            <Text
              style={{marginHorizontal: 10, fontSize: 20, fontWeight: 'bold'}}>
              16
            </Text>
          </View>
        </View>
        <View style={{flexDirection: 'row'}}>
          {/* Kotak Abu 3 dari 4 */}
          <View
            style={{
              margin: 10,
              width: 140,
              height: 70,
              borderRadius: 5,
              backgroundColor: '#eee',
            }}>
            <View style={{flexDirection: 'row', marginVertical: 5}}>
              <View style={{backgroundColor: 'orange', width: 3, height: 20}} />
              <Text style={{marginHorizontal: 5}}>PO</Text>
            </View>
            <Text
              style={{marginHorizontal: 10, fontSize: 20, fontWeight: 'bold'}}>
              12
            </Text>
          </View>

          {/* Kotak Abu 4 dari 4 */}
          <View
            style={{
              margin: 10,
              width: 140,
              height: 70,
              borderRadius: 5,
              backgroundColor: '#eee',
            }}>
            <View style={{flexDirection: 'row', marginVertical: 5}}>
              <View style={{backgroundColor: 'orange', width: 3, height: 20}} />
              <Text style={{marginHorizontal: 5}}>PO Valid</Text>
            </View>
            <Text
              style={{marginHorizontal: 10, fontSize: 20, fontWeight: 'bold'}}>
              32
            </Text>
          </View>
        </View>
      </View>

      {/* Cari dan Filter */}
      <View
        style={{
          flexDirection: 'row',
          marginTop: 15,
          borderRadius: 15,
          height: 50,
          width: '90%',
          marginBottom: 10,
          alignSelf: 'center',
        }}>
        <TextInput
          style={{
            backgroundColor: 'white',
            width: 210,
            height: 48,
            borderTopLeftRadius: 10,
            borderBottomLeftRadius: 10,
          }}
          placeholder="   Telusuri..."
          keyboardType="default"
        />
        <TouchableOpacity
          style={{
            width: 48,
            height: 48,
            position: 'absolute',
            marginLeft: 210,
            backgroundColor: 'white',
            borderTopRightRadius: 10,
            borderBottomRightRadius: 10,
          }}>
          <Image
            source={require('../asset/icon-cari.png')}
            style={{
              height: 20,
              marginVertical: 15,
              width: 20,
              marginLeft: 15,
              backgroundColor: '#F6F8FF',
            }}
          />
        </TouchableOpacity>

        <TouchableOpacity
          style={{
            width: 48,
            height: 48,
            position: 'absolute',
            right: 0,
            backgroundColor: 'white',
            borderRadius: 15,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Image
            source={require('../asset/icon-filter.png')}
            style={{
              height: 30,
              width: 30,
            }}
          />
        </TouchableOpacity>
      </View>
      {/* Cari dan Filter Selesai Di SIni*/}

      <View
        style={{
          flexDirection: 'row',
          marginHorizontal: 40,
          alignSelf: 'center',
          width: '90%',
          backgroundColor: 'yellow',
          height: 50,
        }}>
        <TouchableOpacity
          style={{
            width: '50%',
            height: 36,
            backgroundColor: 'black',
            borderTopLeftRadius: 10,
            borderBottomLeftRadius: 10,
            borderLeftWidth: 10,
          }}
        />
        <TouchableOpacity
          style={{
            width: '50%',
            height: 36,
            backgroundColor: 'pink',
            borderTopRightRadius: 10,
            borderBottomRightRadius: 10,
          }}
        />
      </View>
    </View>
  );
};

export default Home;
